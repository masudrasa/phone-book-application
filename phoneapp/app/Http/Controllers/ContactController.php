<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contacts = App\Contact::orderBy('created_at', 'DESC')->get();;
        return view('contacts.index',compact('contacts'));
    }





    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('contacts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request,['first_name' => 'required','last_name' => 'required','description' => 'required']);

        $contact = new App\Contact();
        $contact->first_name = $request->input('first_name');
        $contact->last_name = $request->input('last_name');
        $contact->description = $request->input('description');

        $contact->save();

        return redirect('/contacts')->with('success','Contact added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $contact = App\Contact::find($id);
        return view('contacts.show',compact('contact'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $contact = App\Contact::find($id);

        return view('contacts.edit',compact('contact'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,['email' => 'email']);

        $contact = App\Contact::find($id);
        $contact->first_name = $request->input('first_name');
        $contact->last_name = $request->input('last_name');
        $contact->description = $request->input('description');
        $contact->email = $request->input('email');
        $contact->phone_number = $request->input('phone');
        $contact->twitter_handle = $request->input('twitter');
        $contact->save();

        return redirect('/contacts')->with('success','Contact modified!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $contact = App\Contact::find($id);
        $contact->delete();
        return redirect('/contacts')->with('success','Contact deleted!');
    }
}
