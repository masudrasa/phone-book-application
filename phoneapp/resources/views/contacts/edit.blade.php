@extends('layouts.master')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"> <span class="float-right"><a href="/contacts" class="btn btn-secondary">Go back</a></span></div>

                <div class="card-body">
                    {{ Form::open(array('action' => ['ContactController@update',$contact->id],'method' => 'PUT')) }}
                    <input name="_method" type="hidden" value="PUT">
                    <div class="form-group">
                        {!! Form::label('first_name','First name') !!}
                        {!! Form::text('first_name', $contact->first_name, ['class' => 'form-control']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('last_name', 'Last') !!}
                        {!! Form::text('last_name', $contact->last_name, ['class' => 'form-control']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('description', 'Beschrijving') !!}
                        {!! Form::textarea('description', $contact->description, ['class' => 'form-control', 'rows' =>3]) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('email', 'Email') !!}
                        {!! Form::text('email', $contact->email, ['class' => 'form-control']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('phone', 'Phone number') !!}
                        {!! Form::text('phone', $contact->phone_number, ['class' => 'form-control']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('twitter', 'Twitter handle') !!}
                        {!! Form::text('twitter', $contact->twitter_handle, ['class' => 'form-control']) !!}
                    </div>


                    <br>
                    {{Form::submit('Edit Contact',['class' => 'btn-primary'])}}

                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>

@endsection