<!DOCTYPE html>
<html lang="en">

<head>

    <!-- Font Awesome -->


    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">

    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet"/>

    <script type="text/javascript" charset="utf8" src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.7.1.min.js"></script>
    <script type="text/javascript" charset="utf8" src="http://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.0/jquery.dataTables.min.js"></script>

</head>

<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-red static-top">
    <div class="container">
        <a class="navbar-brand" href="/">
            PhoneApp
        </a>

    </div>
</nav>
@include('inc.messages')
<div class="container">

    <h1>List of contacts</h1>
    <span class=""><a href="/contacts/create" class="btn btn-success float-right">Add new contact</a></span>


    <table id="dtDashboard" class="table table-dark">
        <thead>
        <tr>

            <th scope="col">Image</th>
            <th scope="col">First Name</th>
            <th scope="col">Last Name</th>
            <th scope="col">Phone Number</th>
            <th scope="col">Email</th>
            <th scope="col">Twitter Handle</th>
            <th scope="col">Details</th>
            <th scope="col">Edit</th>
            <th scope="col">Delete</th>



        </tr>
        </thead>
        <tbody>
        @foreach($contacts as $contact)
            <tr>

                <td><img class="img-thumbnail rounded-circle" width="70px" height="70px" src="http://avatars.io/twitter/{{$contact->twitter_handle}}" alt=""></td>
                <td>{{$contact->first_name}}</td>
                <td>{{$contact->last_name}}</td>
                <td>{{$contact->phone_number}}</td>
                <td>{{$contact->email}}</td>
                <td><a href="http://www.twitter.com/{{$contact->twitter_handle}}" target="_blank">{{$contact->twitter_handle}}</a></td>
                <td><a href="/contacts/{{$contact->id}}" class="btn btn-success">Detail</a>
                <td><a href="/contacts/{{$contact->id}}/edit" class="btn btn-primary">Edit</a></td>
                </td>  <td><form action="{{ action('ContactController@destroy', $contact->id) }}" method="post">
                        {{ csrf_field() }}
                        <input name="_method" type="hidden" value="DELETE">
                        <button type="submit" class="btn btn-danger">Delete</button>
                    </form> </td>

            </tr>
        @endforeach
        </tbody>
    </table>
</div>
<script>

    $(document).ready(function () {
        $('#dtDashboard').DataTable();
        $('#dtDashboard_wrapper').find('label').each(function () {
            $(this).parent().append($(this).children());


        });

        $('#dtDashboard_wrapper .dataTables_filter').find('input').each(function () {
            $('input').attr("placeholder", "Search contacts");
            $('input').removeClass('form-control-sm');

        });

        $('#dtDashboard_wrapper .dataTables_length').addClass('d-flex flex-row');
        $('#dtDashboard_wrapper .dataTables_filter').addClass('md-form');

        $('#dtDashboard_wrapper select').removeClass('custom-select custom-select-sm form-control form-control-sm');
        $('#dtDashboard_wrapper select').addClass('mdb-select');
        $('#dtDashboard_wrapper .mdb-select').materialSelect();
        $('#dtDashboard_wrapper .dataTables_filter').find('label').remove();
    });


</script>
</body>

</html>
