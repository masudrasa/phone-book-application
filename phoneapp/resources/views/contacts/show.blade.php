@extends('layouts.master')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"> <span class="float-right"><a href="/contacts" class="btn btn-secondary">Go back</a></span></div>

                <div class="card-body">
                    <img class="img-thumbnail text-center" width="150px" height="150px" src="http://avatars.io/twitter/{{$contact->twitter_handle}}" alt="">
                    <ul class="list-group">
                        <li class="list-group-item"><i class="fa fa-user fa-2x"></i>     {{$contact->first_name}} {{$contact->last_name}}</li>
                        <li class="list-group-item"><i class="fa fa-envelope fa-2x"></i> <a href="mailto:{{$contact->email}}"> {{$contact->email}}</a></li>
                        <li class="list-group-item"><i class="fa fa-phone fa-2x"></i> <a href="tel:{{$contact->phone_number}}"> {{$contact->phone_number}}</a>  </li>
                        <li class="list-group-item"><i class="fa fa-twitter fa-2x"></i> <a href="http://www.twitter.com/{{$contact->twitter_handle}}" target="_blank"> {{$contact->twitter_handle}}</a>  </li>
                    </ul>
                    <hr>
                    <div class="card bg-light p-3">
                        <span class="float-right"><i class="fa fa-newspaper-o fa-2x"></i><br> {{$contact->description}}</span>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection