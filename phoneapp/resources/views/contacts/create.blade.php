@extends('layouts.master')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"> <span class="float-right"><a href="/contacts" class="btn btn-secondary">Go back</a></span></div>

                <div class="card-body">
                    {{ Form::open(array('action' => 'ContactController@store')) }}

                    <div class="form-group">
                        {!! Form::label('first_name','First name') !!}
                        {!! Form::text('first_name', null, ['class' => 'form-control']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('last_name', 'Last name') !!}
                        {!! Form::text('last_name', null, ['class' => 'form-control']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('description', 'Description') !!}
                        {!! Form::textarea('description', null, ['class' => 'form-control', 'rows' =>3]) !!}
                    </div>


                    <br>
                    {{Form::submit('Add Contact',['class' => 'btn-primary'])}}

                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>

@endsection