##PhoneApp by Masud Rasa



#Web app live on:
http://dev.masudrasa.nl


#How to use
1. When you enter the home page, click on 'Get started!'
2. You will see a list of existing contacts
3. Click on 'Add new contact'
4. Fill in the required input fields(first name, last name and description)
5. Click on 'Add 'Add contact'.
6. The new contact will be added at the top of the list. You won't see an image yet, because the twitter handle hasn't  been filled in.
7. Click on 'Edit', to fill in the additional information(email, phone number and twitter handle) and click on 'Edit contact'.
8. You will now see a twitter profile image based on the twitter handle.
9. Click on 'Delete' contact, if you want to delete the contact from your list.
10. If you enter the first two letter of the contact, the results will be displayed immediately without presssing the 'enter' button.
11. If you want to filter the list based on the column, just click on the column name and the list will be filtered ascending or descending


#Setup Phone App Project from Bitbucket

1.	Make sure you have git installed locally on your computer first
https://git-scm.com/downloads

2.	Find a folder location on your computer and browse with cmd to the folder
3.	Type: git clone https://masudrasa@bitbucket.org/masudrasa/phone-book-application.git
on cmd
4.	Go to the phoneapp folder with cmd: cd phone-book-application/phoneapp
5.	Install composer with cmd in the project folder : composer install
6.	Type in cmd: copy .env.example .env to make a copy from env.example
7.	Type in cmd: php artisan key:generate
8.	Open .env file with notepad and change the following lines:
DB_HOST=
DB_PORT=3306
DB_DATABASE= 
DB_USERNAME= 
DB_PASSWORD=
9.	Type in cmd: php artisan migrate
10.	Test the web application with: php artisan serv

#With this app you can perform the following tasks:
* Create, search, edit a contact.
* Edit the following contact properties: name, last name, description
* Add, edit, or remove the following contact properties: phone number, email, Twitter handle.
* View an alphabetical listing of contacts
* Sort the contact listing by date added


#Home Page

![alt text](https://i.ibb.co/g3XTJ6h/screencapture-dev-masudrasa-nl-2019-05-25-03-15-23.png
)

#Contact list Page
![alt text](https://i.ibb.co/ygV9QPh/screencapture-dev-masudrasa-nl-2019-05-25-03-15-23.png
)

#Contact Detail Page
![alt text](https://i.ibb.co/s2Tmwtq/screencapture-dev-masudrasa-nl-2019-05-25-03-15-23.png
)

##Tools
**Language** : PHP,SQL 

**Frameworks** : Laravel,Bootstrap


